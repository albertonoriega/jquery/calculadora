$(function () {
    // Cuando se hace click en los botones
    $(".botones").click(function () {
        //guardamos en la variable tecla el valor del atributo value
        // es decir el valor de cada boton pulsado
        let tecla = $(this).attr("value");

        // Al div pantalla le "adjuntamos" la variable tecla
        // Así conseguimos que se vayan mostrando los valores clickados
        $(".pantalla").append(tecla);

        // en el input resultado que es de tipo hidden
        // vamos concatenamos cada valor que va tomando tecla
        $(".resultado").val($(".resultado").val() + tecla);
    });

    //Cuando se pulsa el boton igual
    $(".igual").click(function () {

        //Guardamos en la variable texto el valor de todos los numeros y operaciones que se han pulsado
        let texto = $(".resultado").val();

        // En resultado guardamos el resultado de la operación
        $(".resultado").val(eval($(".resultado").val()));

        // En la pantalla mostramos el resultado de la operacion usando eval
        // texto = "10 * 5" => eval(texto) =50
        $(".pantalla").html(eval(texto));
    });

    //Si pulsamos en la C de borrar
    $(".borrar").click(function () {

        //Limpiamos resultado
        $(".resultado").val("");

        // Limpiamos la pantalla
        $(".pantalla").html("");
    });

    //Si pulsamos en la C de borrar
    $(".masmenos").click(function () {

        // pasamos a negativo el resultado
        $(".resultado").val($(".resultado").val() * (-1));

        //Mostramos el resultado en pantalla cambiado de signo
        $(".pantalla").html($(".resultado").val());
    });

});

